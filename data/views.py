from rest_framework import viewsets
from rest_framework import mixins

from .models import Post
from .serializers import PostSerializer

class PostViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  viewsets.GenericViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
